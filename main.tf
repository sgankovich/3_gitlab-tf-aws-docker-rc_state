provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}
terraform {
  backend "remote" {
    organization = "cicd-me-uk"

    workspaces {
      name = "remote_state_gitlab"
    }
  }
}

resource "aws_iam_instance_profile" "xxx_instance_profile" {
  name  = "${var.host_label}_instance_profile"
  role = "AWSBackupDefaultServiceRole"
}



